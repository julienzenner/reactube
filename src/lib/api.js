import axios from 'axios'

const key = 'AIzaSyAslmL98X4bGtGQERik10xkGkhHLJYZDKI'
const fetcher = axios.create({
    baseURL: 'https://youtube.googleapis.com/youtube/v3',
    params: {
        key: key
    }
})

export default fetcher;