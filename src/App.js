import React, {useState} from 'react';
import {Container} from 'react-bootstrap';
import MyNav from './components/MyNav';
import Result from "./components/Result";
import DetailedResult from "./components/DetailedResult";
import './App.css';

// router
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

const App = () => {
    const [videos, setVideos] = useState([]);
    const [selectedVideo, selectVideo] = useState({});
    const [selectedType, selectType] = useState('video');
    return (
        <Container className="p-3">
            <Router>
                <MyNav onResults={setVideos} selectedType={selectedType} selectType={selectType}/>

                <Switch>
                    <Route path="/videos/:videoId">
                        <DetailedResult id={selectedVideo.id}
                                        snippet={selectedVideo.snippet} player={selectedVideo.player}
                                        statistics={selectedVideo.statistics}
                                        type={selectedType}/>
                    </Route>
                    <Route path="/channels/:channelId">
                        <DetailedResult id={selectedVideo.id}
                                        snippet={selectedVideo.snippet}
                                        statistics={selectedVideo.statistics}
                                        type={selectedType}/>
                    </Route>
                    <Route path="/search/:type/:search">
                        <>
                            {videos.map(v => {
                                const {
                                    title, description, thumbnails,
                                    channelTitle, publishTime
                                } = v.snippet;

                                let idResult = v.id.videoId;
                                if (selectedType === 'channel') {
                                    idResult = v.id.channelId;
                                }

                                return (<Result
                                    videoId={idResult}
                                    thumbnail={thumbnails.high}
                                    description={description}
                                    channelTitle={channelTitle}
                                    publishTime={publishTime}
                                    title={title}
                                    selectVideo={selectVideo}
                                    type={selectedType}/>);
                            })}
                        </>
                    </Route>
                    <Route path="/">
                        Merci d'effectuer une recherche...
                    </Route>
                </Switch>
            </Router>
        </Container>
    );
};

export default App;
